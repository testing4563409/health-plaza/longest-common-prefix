function longestCommonPrefix(strs) {
    
    if (!strs.length) {
        return "";
    }
    
    let prefix = "";
    
    for (let i = 0; i < strs[0].length; i++) {
        
        const char = strs[0][i];
        
        for (let j = 1; j < strs.length; j++) {
            
            if (i >= strs[j].length || strs[j][i] !== char) {
                return prefix;
            }
            
        }
        
        prefix += char;
    }

    return prefix;
}

// Test cases
const strs1 = ["flower", "flow", "flight"];
console.log(longestCommonPrefix(strs1));

const strs2 = ["dog", "racecar", "car"];
console.log(longestCommonPrefix(strs2));

const strs3 = ["abc", "abcd", "abcde"];
console.log(longestCommonPrefix(strs3));